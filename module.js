import PuppeteerBrowser from "./src/PuppeteerBrowser";
import PuppeteerWebshots from "./src/PuppeteerWebshots";

export {PuppeteerBrowser, PuppeteerWebshots};
