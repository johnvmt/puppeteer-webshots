import EventEmitter from 'wolfy87-eventemitter';

class PuppeteerWebshots extends EventEmitter {
	constructor(browserController) {
		super();

		const self = this;

		self.browserController = browserController;
		self.canceled = false;

		self._jobStarted = false;
		self._pageRequest = null;
		self._page = null;
	}

	async timedScreenshots(url, viewportOptions, screenshotTimes) {

		const self = this;

		return new Promise(async function(resolve, reject) {
			if(self._jobStarted)
				reject('job_in_progress');
			else {
				self.emit('status', 'queued');
				self._jobStarted = true;
				self._pageRequest = self.browserController.newPage(async function(page) {

					try {
						if(self.canceled)
							reject('job_canceled');
						else {
							self.emit('status', 'started');

							self._page = page;

							page.on('error', (error) => {
								self.emit('error', error);
								self.cancel();
							});

							page.once('close', () => {
								self.emit('status', 'closed');
								self.emit('close');
							});

							await page.goto(url);

							if(self.canceled)
								reject('job_canceled');
							else {
								page.setViewport(Object.assign({width: 1920, height: 1080}, (viewportOptions || {})));

								for(let index = 0; index < screenshotTimes.length; index++) {
									let delay = (index === 0) ? screenshotTimes[0] : screenshotTimes[index] - screenshotTimes[index - 1];
									await self._delay(delay);
									if(self.canceled) {
										reject('job_canceled');
										break;
									}
									else {
										let screenshot = await page.screenshot();
										if(self.canceled) {
											reject('job_canceled');
											break;
										}
										else {
											self.emit('screenshot', {
												time: screenshotTimes[index],
												screenshot: screenshot
											});
										}
									}
								}

								resolve(self.cancel());
							}
						}
					}
					catch(error) {
						self.emit('error', error);
						self.cancel();
					}
				});
			}
		});
	}

	async cancel() {
		const self = this;

		self.canceled = true;

		if(typeof self._pageRequest === 'object' && self._pageRequest !== null && typeof self._pageRequest.cancel === 'function') {
			try {
				self._pageRequest.cancel();
			}
			catch(error) {
				self.emit('error', error);
			}
		}

		if(self._page !== null && !self._page.isClosed())
			await self._page.close();
	}

	async _delay(timeout) {
		return new Promise((resolve) => {
			setTimeout(resolve, timeout);
		});
	}


}

export default PuppeteerWebshots;
