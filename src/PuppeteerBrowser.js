import puppeteer from 'puppeteer';
import EventEmitter from 'wolfy87-eventemitter';
import PuppeteerScreenshots from './PuppeteerWebshots';
import JobsQueue from 'jobs-queue';

class PuppeteerBrowser extends EventEmitter {
	constructor(options, browserOptions) {
		super();

		this.options = Object.assign({
			maxPages: 1
		}, (options || {}));

		this.browserOptions = Object.assign({
			headless: true,
			args: ['--no-sandbox', '--disable-setuid-sandbox'],
			ignoreHTTPSErrors: true,
			dumpio: false
		}, (browserOptions || {}));

		this.browser = null;
		this.queue = JobsQueue({maxSimultaneous: this.options.maxPages});

		this._browserStarting = false;
	}

	async startBrowser() {
		const self = this;

		return new Promise(async function(resolve, reject) {
			if(self.browser !== null)
				resolve(self.browser);
			else {
				self.once('browserLaunched', function() {
					resolve(self.browser);
				});

				if(!self._browserStarting) {
					self._browserStarting = true;
					try {
						self.browser = await puppeteer.launch(self.browserOptions);
						self.emit('browserLaunched');
					}
					catch(error) {
						self.emit('error', error);
					}
				}
			}
		});
	}

	newPage(callback) {
		const self = this;

		return self.queue.enqueue(async function(done) {
			let browser = await self.startBrowser();
			let browserPage = await browser.newPage();
			browserPage.once('close', function() {
				done();
			});
			callback(browserPage);
		});
	}
}

export default PuppeteerBrowser;
