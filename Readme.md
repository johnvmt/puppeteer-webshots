## Puppeteer Webshots

### Example

	const {PuppeteerBrowser, PuppeteerWebshots} = require('puppeteer-webshots');
	
	let browser = new PuppeteerBrowser({maxPages: 2});
	let screenshots = new PuppeteerWebshots(browser);
	
	screenshots.timedScreenshots(
		'http://myurl.com',
		{width: 1920, height: 1080},
		[1000, 2000, 4000, 20000]
	)
	.then(() => console.log("DONE"))
	.catch((error) => console.error(error));
	
	// Cancel it manually
	setTimeout(function() {
		screenshots.cancel();
	}, 20000);
	
	screenshots.on('screenshot', function({time, screenshot}) {
		console.log(time, screenshot);
	});
