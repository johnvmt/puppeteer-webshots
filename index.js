require = require('esm')(module);

module.exports = {
	PuppeteerWebshots: require('./module').PuppeteerWebshots,
	PuppeteerBrowser: require('./module').PuppeteerBrowser
};
